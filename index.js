//USING EXPRESS JS FRAMEWORK, to create a server an easier way using few lines.
//No need to use PARSE and STRINGIFY.
//Better than the regular NODE.JS method.
//Lesser statements to avoid confusions.


///GET REQUEST///
	//require directive is used to load the express module/package that we installed.
	//allows us to access the methods and FX in easily creating our server.
	const express = require("express"); //"express" is not user defined.
	//This creates an express application and stores this in a const called app.
	//In other words, const app is our server, a lot easier than the usual node.js
	//this is like createServer() but the shortest version.
	const app = express();
	//"use" method means will use a middleware that allows your app to read json data.
	//converts automatically to json format.
	//no need to use .parse and .stringify. 
	//set up for allowing the server to handle data from requests.
	app.use(express.json());
	//For our application server to run, we need a port to listen to example port 4000/3000
	const port = 3000;
	//Express has methods corresponding to each HTTP method.
	//  '/' corresponds with our base URI e.g. localhost:3000/
	//Syntax: app.httpMethod("/endpoing", (req, rest) => {})
	app.get('/', (req, res) => {
		//res.send uses the express JS module's method to send a response back to the client
		res.send("Hello World"); 
	})


///POST REQUEST///
//THis route expects to receive a POST request at the URI /endpoint "/hello"
	app.post("/hello", (req, res)=> {
		//req.body contains the contents/data of the request.
		//so on postman we used POST, body, raw, JSON then type {"firstname": "John", "lastName": "Doe"}
		// then send, and you will
		//see "hello there" on postman and "John" and "Doe" will be printed on the TERMINAL of CMD.
		//"John" "Doe" is already in OBJECT no need to parse.
		console.log(req.body);
		//${req.body.firstName} ${req.body.lastName} will receive the objects "john" and "Doe" from CMD/Terminal
		res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)  ////
	});

///CRUD Operation (Create/post, read/post, update/patch/put, delete/delete)
/*
	SCENARIO:

		We want to create a simple users database that will perform CRUD operations 
		based on the client request. The following routes should peform its functionality:
*/
	//Mock database for our users
	// let users = []; ito nung una
	let users = [			//ginawa natin to para sa example ni PUT sa SCENARIO
		{
		username: "janedoe",
		password: "hane123"
		},
		{
			username: "johnsmith",
			password: "john123"
		}
	];	
	//==========POST==========
	//This route [] expects to receive a POST request at the URI given "/signup"
	//This will create a user object in the 'users' array that mirrors real world registration:
		//All fields are required to be filled out for this scenario.
	app.post("/signup", (req, res) => {
		console.log(req.body);
		//We will use if condition to see if all fields are filled out.
		//If contents of "req.body" w/ property of "username" and "password" is not empty.
		if(req.body.username !== "" && req.body.password !== "" && req.body.username !==
			undefined && req.body.password !== undefined){
			//To store the req.body sent via Postman in the users array 
			//we will use "PUSH" method.
			//User object will be saved in our USERS array, the username and password.
			users.push(req.body);
			res.send(`User ${req.body.username} successfully registered!`);
		} //if the username and password are not provided or not complete print the else statement.
		else{
			res.send("Please input BOTH username and password");
		}
	});

	//==========GET==========
	//This route expects to receive a GET request at the URI '/users'.
	//This will retrieve all the users stored in variable [] through post.
	//This will allow us to view what are stored currently in the array through POST.
	//On post man you will still see an [] array, this is because the server
	//refreshes when we save the file changes so under the s34 folder on POSTMAN, 
	//resend the sign up a user GET.
	//if you will do this the [] will now have an input item.
	app.get("/users", (req, res) => {
		res.send(users);
	});

	//==========PUT/UPDATE==========
	//This route expects to receive a PUT(update) request at the URI "/change-password"
	//This will update the password of a user that matches the information provided in the client/Postman
	//We will use the "username" to search who's user will have a password UPDATE.
	app.put("/change-password", (req, res) => {
		//creates a variable to store the message to be sent back to the client.
		let message;
		//related to users with janedoe and johndoe, we will search for JOHN we will use for loop.
		for(let i = 0; i < users.length; i++) {
			//if the username provided by the client/postman and username of the current element
			//or object in the loop is the same.
			if(users[i].username === req.body.username){
				//changes the password of the user found by the loop into the password provided
				//by the client/user/postman	
				users[i].password = req.body.password;
				message = `User ${req.body.username}'s password has been updated`;
				//use BREAK, once what we are looking for is found and updated, break the search
				break;
		 }  else{
		 		//If nothing was found.
		 		message = "User does not exist!";
			}
		}	
		//sends a response back to the clien/postman once the password has been updated or if a user is ot found.	
		res.send(message);	
	})

	//==========DELETE==========
	// This route expects to receive a DELETE request at the URI "/delete-user"
	// This will remove the user from the array for deletion
		app.delete("/delete-user", (req, res) => {
		//creates a variable to store the message to be sent back to the client.
		let message;
		//related to users with janedoe and johndoe, we will search for JOHN we will use for loop.
		for(let i = 0; i < users.length; i++) {
			//if the username provided by the client/postman and username of the current element
			//or object in the loop is the same.
			if(users[i].username === req.body.username){
				//Splice method manipulates the array and removes the user object from the 'users'
				//array based on it's index.
				users.splice(users[i], 1)  //1 means number of elements to remove.
				message = `User ${req.body.username}'s name has been deleted`;
				//use BREAK, once what we are looking for is found and updated, break the search
				break;
		 }  else{
		 		//If nothing was found.
		 		message = "User does not exist!";
			}
		}	
		//sends a response back to the clien/postman once the password has been updated or if a user is ot found.	
		res.send(message);	
	})

//Returns or notifies a message to confien that the server is running in the terminal
//or getting an error. 
	app.listen(port, () => console.log(`Server is running at port: ${port}`)); //important lagi to sa dulo si listener.






////THIS IS THE SAME AS ABOVE I JUST REMOVED ALL THE COMMENTS/////
////////////////////////////////
//USING EXPRESS JS FRAMEWORK, to create a server an easier way using few lines.
//No need to use PARSE and STRINGIFY.
//Better than the regular NODE.JS method.
//Lesser statements to avoid confusions.


///GET REQUEST///
	const express = require("express"); 
	const app = express();
	app.use(express.json());
	const port = 3000;
	app.get('/', (req, res) => {
		res.send("Hello World"); 
	})


///POST REQUEST///
	app.post("/hello", (req, res)=> {
		console.log(req.body);
		res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)  
	});

///CRUD Operation (Create/post, read/retrieve/get, update/patch/put, delete/delete)
	let users = [		
		{
		username: "janedoe",
		password: "hane123"
		},
		{
			username: "johnsmith",
			password: "john123"
		}
	];	
	//==========POST/CREATE==========
	app.post("/signup", (req, res) => {
		console.log(req.body);
		if(req.body.username !== "" && req.body.password !== "" && req.body.username !==
			undefined && req.body.password !== undefined){
			users.push(req.body);
			res.send(`User ${req.body.username} successfully registered!`);
		}
		else{
			res.send("Please input BOTH username and password");
		}
	});

	//==========GET/RETRIEVE/READ==========
	app.get("/users", (req, res) => {
		res.send(users);
	});

	//==========PUT/UPDATE==========
	app.put("/change-password", (req, res) => {
		let message;
		for(let i = 0; i < users.length; i++) {
			if(users[i].username === req.body.username){	
				users[i].password = req.body.password;
				message = `User ${req.body.username}'s password has been updated`;
				break;
		 }  else{
		 		message = "User does not exist!";
			}
		}		
		res.send(message);	
	})

	//==========DELETE==========
		app.delete("/delete-user", (req, res) => {
		let message;
		for(let i = 0; i < users.length; i++) {
			if(users[i].username === req.body.username){
				users.splice(users[i], 1)  
				message = `User ${req.body.username}'s name has been deleted`;
				break;
		 }  else{
		 		message = "User does not exist!";
			}
		}		
		res.send(message);	
	})

	app.listen(port, () => console.log(`Server is running at port: ${port}`)); ///this is important lagi sa dulo.










































/*
npm init sa cmd
this will create a touch file called `package.json' on your sublime.

===========================================
$ npm init
===========================================
This utility will walk you through creating a package.json file.
It only covers the most common items, and tries to guess sensible defaults.

See `npm help init` for definitive documentation on these fields
and exactly what they do.

Use `npm install <pkg>` afterwards to install a package and
save it as a dependency in the package.json file.

Press ^C at any time to quit.
package name: (d1)

===========================================
-after this one shows- press enter several times hanggang dumating sa 
'is this OK? (yes) 
tapos enter uli once more.
===========================================
*/
//then type on cmd//
//nmp install express//

//then we added//
//.gitignore	- since di natin isasama lahat ng packages.
//ignores all the file that we will place inside this folder, will not be pushed on remote repo.

//when we did the npm init a folder called node_modules was created, we will place this inside the .gitignore file to ignore it.
//save the .gitignore folder and then git init so the node_modules file will be ignored and grayed out.